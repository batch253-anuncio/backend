const { Schema, model } = require("mongoose");

const taskSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: "pending",
  },
});

const Task = model("Task", taskSchema);
module.exports = Task;
