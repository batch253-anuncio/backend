const Task = require("../model/task.model");

// create a task
const createTask = async (request, response) => {
  const { name } = request.body;

  try {
    if (!name || !name.trim())
      return response
        .status(400)
        .send("Task is invalid, either empty or spaces are inputted");

    const checkDuplicate = await Task.findOne({ name });

    if (checkDuplicate)
      return response.status(400).send("Duplicate Task found");

    await Task.create({ name });
    return response.status(201).send("Task created");
  } catch (error) {
    console.log(error);
    return response.status(400).send(error);
  }
};

// get all task
const getaAllTask = async (request, response) => {
  try {
    const allTasks = await Task.find();

    if (!allTasks.length)
      return response.status(201).send("No Tasks at the moment");

    return response.status(201).json({ status: "success", data: allTasks });
  } catch (error) {
    console.log(error);
    return response.status(400).send(error);
  }
};

// update a Task
const updateTask = async (request, response) => {
  const { body } = request;
  const { id } = request.params;

  try {
    if (!body.name || !body.name.trim())
      return response.status(400).send("no data received");

    if (!id) return response.status(400).send("TaskID not Found");

    const updateTask = await Task.findByIdAndUpdate(id, body, { new: true });
    console.log(updateTask);

    if (updateTask === null)
      return response.status(400).send(`No task found with ${body.name}`);

    return response
      .status(201)
      .json({ status: "data updated", data: updateTask });
  } catch (error) {
    console.log(error);
    return response.status(400).send(error);
  }
};

// delete a task
const deleteTask = async (request, response) => {
  const { id } = request.params;

  try {
    const deleteTask = await Task.findByIdAndDelete(id);

    return response
      .status(200)
      .json({ status: "success", message: `${deleteTask.name} deleted` });
  } catch (error) {
    console.log(error);
    return response.status(400).send(error);
  }
};

// update task status
const updateStatus = async (request, response) => {
  const { id } = request.params;

  try {
    if (!id) return response.status(404).send("Id Not Found");
    const updateStatus = await Task.findByIdAndUpdate(
      id,
      { status: "complete" },
      { new: true }
    );
    return response.status(201).send(updateStatus);
  } catch (error) {
    console.log(error);
    resposnse.send(error);
  }
};

// find task by id
const getTaskById = async (request, response) => {
  const { id } = request.params;

  try {
    if (!id) return response.status(404).send("Id Not Found");
    const getTask = await Task.findById(id).exec();
    return response.status(201).send(getTask);
  } catch (error) {
    console.log(error);
    resposnse.send(error);
  }
};

module.exports = {
  createTask,
  getaAllTask,
  updateTask,
  deleteTask,
  updateStatus,
  getTaskById,
};
