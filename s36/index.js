const express = require("express");
const mongoose = require("mongoose");

const taskRouter = require("./routes/task.route");
const connectDB = require("./utils/connect");
const logger = require("./utils/logger");
const app = express();

process.on("uncaughtException", (error) => {
  logger.error("UNHANDLED EXCEPTION! Shutting down...");
  logger.error(error.name, error.message);
  process.exit(1);
});

const PORT = 4000;

connectDB();

// middlewares
app.use(express.json());

// routes
app.use("/tasks", taskRouter);

mongoose.connection.once("open", () => {
  app
    .listen(PORT, () =>
      logger.info(`Server running at http://localhost:${PORT}`)
    )
    .on("error", () => {
      process.exit(1);
    });
});

mongoose.connection.on("error", (error) => {
  logger.error(
    `${error.no}: ${error.code}\t${error.syscall}\t${error.hostname}`,
    "mongoErrLog.log"
  );
});

module.exports = app;
