const mongoose = require("mongoose");
const logger = require("./logger");

const connectDB = async () => {
  const databaseUri =
    "mongodb+srv://admin:admin123@b253-anuncio.kilox2p.mongodb.net/s36?retryWrites=true&w=majority";

  try {
    mongoose.set("strictQuery", false);
    await mongoose.connect(databaseUri);
    logger.info("DB connection successful.");
  } catch (error) {
    logger.error("Could not connect to DB.");
    logger.error(error);
    process.exit(1);
  }
};

module.exports = connectDB;
