const taskRouter = require("express").Router();
const taskController = require("../controllers/task.controller");

taskRouter
  .route("/")
  .get(taskController.getaAllTask)
  .post(taskController.createTask);

taskRouter
  .route("/:id")
  .get(taskController.getTaskById)
  .put(taskController.updateTask)
  .delete(taskController.deleteTask);

taskRouter.patch("/:id/complete", taskController.updateStatus);

module.exports = taskRouter;
