directory = [
  {
    name: "Brandon",
    email: "brandon@mail.com",
  },
  {
    name: "Jobert",
    email: "jobert@mail.com",
  },
];

let http = require("http");
http
  .createServer((request, response) => {
    if (request.url == "/users" && request.method == "POST") {
      response.writeHead(200, { "Content-Type": "application/json" });
      response.write(JSON.stringify(directory));
      response.end();
    } else if (request.url == "/users" && request.method == "POST") {
      console.log(request);

      request.on("data", (data) => {
        let newUser = data;
        console.log(data);
        const updatedUser = {
          email: newUser.email,
          password: newUser.password,
        };

        directory.push(updatedUser);
        response.statusCode(200).json({ data: directory });
      });
    }
  })
  .listen(4000);

console.log("Server running at locahost:4000");
