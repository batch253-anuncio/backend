let http = require("http");
let port = 4000;

const users = [
  {
    name: "james",
    charname: "cloudsky",
  },
];

http
  .createServer(function (request, response) {
    if (request.url == "/users" && request.method == "GET") {
      response.status(200).json({ data: JSON.stringify(users) });
    }
  })
  .listen(port);
console.log("Running at localhost: 4000");
