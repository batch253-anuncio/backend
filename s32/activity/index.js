const http = require("http");
const port = 4000;

const server = http.createServer((request, response) => {
  if (request.url === "/" && request.method === "GET") {
    response.writeHead(200, { "Content-Type": "plain/text" });
    response.end("Welcome to booking system");
  } else if (request.url === "/profile" && request.method === "GET") {
    response.writeHead(200, { "Content-Type": "plain/text" });
    response.end("Welcome to your profile");
  } else if (request.url === "/courses" && request.method === "GET") {
    response.writeHead(200, { "Content-Type": "plain/text" });
    response.end("Here's our courses available");
  } else if (request.url === "/addCourse" && request.method === "POST") {
    response.writeHead(200, { "Content-Type": "plain/text" });
    response.end("Add course to our resources");
  } else if (request.url === "/updateCourse" && request.method === "PUT") {
    response.writeHead(200, { "Content-Type": "plain/text" });
    response.end("Update a course to our resource");
  } else if (request.url === "/archiveCourse" && request.method === "DELETE") {
    response.writeHead(200, { "Content-Type": "plain/text" });
    response.end("Archived course to our resource");
  } else {
    response.writeHead(404, { "Content-Type": "plain/text" });
    response.end("Page not found");
  }
});

server.listen(port, () => {
  console.log(`Server listening to port ${port}`);
});
