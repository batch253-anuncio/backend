// console.log("Wasap!");

// Array Mutators

/*
    1. Mutator Methods
        - seeks to modify the contents of an array.
        - mutator methods are functions that mutate or change an array after they are created. These methods manipulate the original array by perfoming various tasks such as adding or removing elements.
*/

let fruits = ["Apple", "Banna", "Orange", "Mango"];

/*
 	push()
 		-adds an element at the end of an array and it returns array's Length
*/

console.log("Current Fruits Array:");
console.log(fruits);

// adding element/s
let fruitLength = fruits.push("Pineapple");
console.log(fruitLength);
console.log("Mutated Array using push() method");
console.log(fruits);

fruits.push("Avocado", "Jackfruit", "Guava", "Pomelo");
console.log(fruits);

/* 
    pop()
        - removes the last elementin our array and returns the removed element (when valu is passed in another variable)
*/

let removeFruit = fruits.pop();
console.log(removeFruit);
console.log("Mutated Array using pop() method");
console.log(fruits);
fruits.pop();
console.log(fruits);

/* 
    unshift()
        - adds one or more elements AT THE BEGINNING of an array and returns the length of the array(when value is passed in another variable).

        Syntax:
            arrayName.unshift(element);
            arrayName.unshift(elementA, elementB);
*/

let unshiftLength = fruits.unshift("Lemon");
console.log(unshiftLength);
console.log("Mutated Array using unshift() method");
console.log(fruits);

fruits.unshift("Kiwi", "Strawberry");
console.log(fruits);

/* 
    shift()
        - removes an element AT THE BEGINNING of our array and returns the removed elementwhen stored in avariable.
*/

let shiftFruit = fruits.shift();
console.log(shiftFruit);
console.log("Mutated Array using shift() method");
