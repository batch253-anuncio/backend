//Do not modify
//For exporting to test.js

function addNum(num1, num2) {
  const total = num1 + num2;
  console.log("displayed sum of " + num1 + " and " + num2);
  console.log(total);
}

addNum(5, 15);

function subNum(num1, num2) {
  const total = num1 - num2;
  console.log("displayed sum of " + num1 + " and " + num2);
  console.log(total);
}

subNum(20, 5);

function multiplyNum(num1, num2) {
  const total = num1 * num2;
  return total;
}

function divideNum(num1, num2) {
  const total = num1 / num2;
  return total;
}

const product = multiplyNum(50, 10);
const qoutient = divideNum(50, 10);

console.log("The product of 50 and 10:");
console.log(product);

console.log("The qoutient of 50 and 10:");
console.log(qoutient);

function getCircleArea(radius) {
  const area = radius ** 2 * Math.PI;
  return area;
}

const circleArea = getCircleArea(15);
console.log("The result of getting the area of a circle with 15 radius: ");
console.log(circleArea.toFixed(2));

function getAverage(num1, num2, num3, num4) {
  const totalAve = (num1 + num2 + num3 + num4) / 4;
  return totalAve;
}

const averageVar = getAverage(20, 40, 60, 80);
console.log("The average of 20, 40, 60 and 80: ");
console.log(averageVar);

function checkIfPassed(score, totalScore) {
  const total = (score * 100) / totalScore;

  const isPassed = total > 75;
  return isPassed;
}

const isPassingScore = checkIfPassed(38, 50);
console.log("Is 38/50 a passing score?");
console.log(isPassingScore);

console.log();

try {
  module.exports = {
    addNum,
    subNum,
    multiplyNum,
    divideNum,
    getCircleArea,
    getAverage,
    checkIfPassed,
  };
} catch (err) {}
