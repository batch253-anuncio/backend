db.inventory.insertMany([
  {
    name: "Javascript for Beginners",
    author: "James Doe",
    price: 5000,
    stocks: 50,
    publisher: "JS Publishing House",
  },
  {
    name: "HTML and CSS",
    author: "John Thomas",
    price: 2500,
    stocks: 38,
    publisher: "NY Publishers",
  },
  {
    name: "Web Development Fundamentals",
    author: "Noah Jimenez",
    price: 3000,
    stocks: 10,
    publisher: "Big Idea Publishing",
  },
  {
    name: "Java Programming",
    author: "David Michael",
    price: 10000,
    stocks: 100,
    publisher: "JS Publishing House",
  },
]);

// Comparison Query Operators
// $gt operator - Matches the values that are greater than a specified value.
// $gte operator - Matches the values that are greater than or equal to a specified value.
/*
	Syntax:
		for $gt:
			db.collectionName.find(
				{
					"field": { $gt: "Value"}
				}
		);
		for $gte:
			db.collectionName.find({ "field": { $gte; "value"}});
	
*/

db.inventory.find({
  stocks: { $gte: 50 },
});

db.inventory.find({
  stocks: { $gt: 50 },
});

// $lt operator - Matches the values that are less than a specified value.
// $lte operator - Matches the values that are less than or equal to a specified value.
/*
	Syntax:
		for $lt:
			db.collectionName.find(
				{
					"field": { $lt: "Value"}
				}
		);
		for $lte:
			db.collectionName.find(
				{
					"field": { $lte: "Value"}
				}
		);
*/

db.inventory.find({
  stocks: { $lte: 50 },
});

db.inventory.find({
  stocks: { $lt: 50 },
});

// $ne operator - Matches all values that are "NOT" equal to a specified value.
/*
	for $ne:
		db.collectionName.find(
			{
				"field": { $ne: "Value"}
			}
	);
*/

db.inventory.find({
  stocks: { $ne: 50 },
});

// $eq operator - Matches all values that are equal to a specified value.
/*
	for $eq:
		db.collectionName.find(
			{
				"field": { $eq: "Value"}
			}
	);
*/

db.inventory.find({
  stocks: { $eq: 50 },
});

// $in operator - Matches any of the values specified in an array.
/*
	for $in:
		db.collectionName.find(
			{
				"field": { $in: "Value"}
			}
	);
*/
db.inventory.find({
  price: { $in: [10000, 5000] },
});

// $nin operator - Matches none of the values specified in an array.
/*
	for $nin:
		db.collectionName.find(
			{
				"field": { $nin: "Value"}
			}
	);
*/
db.inventory.find({
  price: { $nin: [10000, 5000] },
});

db.inventory.find({
  price: { $gt: 2000 } && { $lt: 4000 },
});

// Mini Activity:
/*
	Mini-Activity:
		1. In the inventory collection, return all the documents that have the price equal or less than 4000.
		2. In the inventory collection, return all the documents that have stocks of 50 and 100

*/

db.inventory.find({
  price: { $lte: 4000 },
});

db.inventory.find({
  price: { $eq: 50 } && { $eq: 100 },
});

// Logical Query Operator
/*
	$or operator - Joins query clauses with a logical OR and returns all documents that match the conditions of either clause.
	
		Syntax:
			Syntax:
			        db.collectionName.find({
			            $or: [
			                { "fieldA": "valueA"},
			                { "fieldB": "valueB"}
			            ]
			        });
*/

db.inventory.find({
  $or: [{ name: "HTML and CSS" }, { publisher: "JS Publishing House" }],
});

db.inventory.find({
  $or: [
    { author: "James Doe" },
    {
      price: {
        $lte: 5000,
      },
    },
  ],
});

db.inventory.find({
  $and: [
    {
      stocks: {
        $ne: 50,
      },
    },
    {
      stocks: {
        $ne: 5000,
      },
    },
  ],
});

db.inventory.find(
  {
    publisher: "JS Publishing House",
  },
  {
    name: false,
  }
);

db.inventory.find({
  author: {
    $regex: "M",
  },
});

db.inventory.find({
  author: {
    $regex: "M",
    $options: "$e",
  },
});
