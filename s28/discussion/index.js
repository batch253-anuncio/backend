// CRUD Operations
/*
		    - CRUD operations are the heart of any backend application.
		    - Mastering the CRUD operations is essential for any developer.
		    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
		    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
		*/

db.users.instertOne({
  firstName: "James",
  lastName: "Anuncio",
  mobileNumer: "0912389124",
  email: "aksol@email.com",
  company: "Zuit Bootcamp",
});

db.user.insertMany([
  {
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    contact: { phone: "87654321", email: "stephenhawking@mail.com" },
    courses: { courses: ["Python", "React", "PHP", "CSS"], department: "none" },
  },
  {
    firstName: "Neil",
    lastName: "Armstrong",
    age: 82,
    contact: { phone: "97654321", email: "neilarmstrong@mail.com" },
    courses: { courses: ["React", "Laravel", "Sass"], department: "none" },
  },
]);

db.courses.insertMany([
  {
    name: "Javascript 101",
    price: 5000,
    description: "Introduction to Javascript",
    isActive: true,
  },
  {
    name: "HTML 101",
    price: 2000,
    description: "Introduction to HTML",
    isActive: true,
  },
]);
