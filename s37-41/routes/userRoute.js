const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for checking if the user's email already exists in the database
// Invokes the checkEmailExists function from the controller file to communicate with our database
// Passes the "body" property of our "request" object to the corresponding controller function
router.post("/checkEmail", (req, res) => {
  // The full route to access this is "http://localhost:4000/users/checkEmail" where the "/users" was defined in our "index.js" file
  // The "then" method uses the result from the controller function and sends it back to the frontend application via the "res.send" method
  userController
    .checkEmailExists(req.body)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

// Route for user registration

router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

router.get("/details", (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  userController
    .getProfile(req.body)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

router.get("/", (req, res) => {
  userController
    .getUsers()
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
});

router.post("/enroll", auth.verify, (req, res) => {
  let data = {
    courseId: req.body.courseId,
  };
  const userData = auth.decode(req.headers.authorization);

  userController
    .enroll(data, userData)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => {
      res.status(500).send(err.message);
    });
});

module.exports = router;
