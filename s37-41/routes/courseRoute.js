const router = require("express").Router();
const auth = require("../auth");
const courseController = require("../controllers/courseController");

//Route for creating a course

router.post("/", auth.verify, (req, res) =>
  courseController
    .addCourse(req)
    .then((resultFormController) => res.send(resultFormController))
    .catch((err) => {
      res.send(err);
    })
);

// ==================== LECTURE =====================

router.get("/all", auth.verify, (req, res) => {
  // need middleware auth
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      courseController
        .getAllCourses()
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => {
          console.error(err);
          res.status(400).send({ error: "Error getting all courses" });
        });
    } else {
      res.send(false);
    }
  } catch (err) {
    console.error(err);
    res.status(400).send({ error: "Error decoding authorization header" });
  }
});

// Route for retrieving all the ACTIVE courses
// Middleware for verifying JWT is not required because users who aren't logged in should also be able to view the courses
router.get("/", (req, res) => {
  courseController
    .getAllActive()
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => {
      console.error(err);
      res.status(500).send({ error: "Internal server error" });
    });
  x``;
});

router.get("/:id", (req, res) => {
  courseController
    .getCourse(req.params.id, req.body)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

router.put("/:id", auth.verify, (req, res) =>
  courseController
    .updateCourse(req.params.id, req.body)
    .then((result) => res.send(result))
    .catch((err) => {
      console.log(error);
      return res.send(err);
    })
);

router.patch("/:id", auth.verify, (req, res) => {
  const result = auth.decode(req.headers.authorization);

  courseController
    .archiveCourse(req.params.id, req.body, result)
    .then((result) => res.send(result))
    .catch((err) => {
      console.log(err);
      return res.send(err);
    });
});

module.exports = router;
