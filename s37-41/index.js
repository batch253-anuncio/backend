const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");
const userRouter = require("./routes/userRoute");
const courseRouter = require("./routes/courseRoute");

// Creates an "app" variable that stores the result of the "express" function that initializes our express application and allows us access to different methods that will make backend creation easy
const app = express();
const port = process.env.PORT || 4000;
const db = mongoose.connection;

// Connect to MOngoDB database
mongoose.connect(
  "mongodb+srv://admin:admin123@b253-anuncio.kilox2p.mongodb.net/course-bookingAPI?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

db.once("open", () => console.log("Now connected to MongoDB Atlas."));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Defines the "/users" string to be included for all user routes defined in the "user" route file
//localhost: 4000 / users / checkEmail;
app.use("/courses", courseRouter);
app.use("/users", userRouter);

if (require.main === module) {
  app.listen(port, () => {
    console.log(`API is now online on port ${port}`);
  });
}

module.exports = app;
