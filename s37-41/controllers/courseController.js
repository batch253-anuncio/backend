const Course = require("../models/course");
const auth = require("../auth");

module.exports.addCourse = (req) => {
  const result = auth.decode(req.headers.authorization);

  if (!result.isAdmin) throw new Error("You are not an admin");
  else return Course.create(req.body);
};

module.exports.getAllCourses = () => {
  return Course.find({})
    .then((result) => result)
    .catch((err) => {
      console.error(err);
      throw new Error("Error adding course.");
    });
};

module.exports.getAllActive = () => {
  return Course.find({ isActive: true })
    .then((result) => result)
    .catch((err) => err);
};

module.exports.updateCourse = (id, data) =>
  Course.findByIdAndUpdate(id, data, { new: true })
    .exec()
    .then((result) => (result ? result : new Error("No course in the ID")))
    .catch((err) => console.log(err));

module.exports.archiveCourse = (id, data, result) => {
  if (!result.isAdmin) throw new Error("You are not an admin");

  return Course.findByIdAndUpdate(id, data, { new: true })
    .exec()
    .then((result) => (result ? result : false))
    .catch((err) => console.log(err));
};
