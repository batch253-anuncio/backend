// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const User = require("../models/user");
const brcypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/course");

// Check if the email already exists
/*
Steps: 
1. Use mongoose "find" method to find duplicate emails
2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
/

module.exports.checkEmailExists = (reqBody) => {
  // The result is sent back to the frontend via the "then" method found in the route file
  return User.find({ email: reqBody.email })
    .then((result) => {
      // The "find" method returns a record if a match is found
      if (result.length > 0) {
        return true;
        // No duplicate email found
        // The user is not yet registered in the database
      } else {
        return false;
      }
    })
    .catch((err) => err);
};

// User registration
/
            Steps:
            1. Create a new User object using the mongoose model and the information from the request body
            2. Make sure that the password is encrypted
            3. Save the new User to the database
*/

module.exports.registerUser = async (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: await brcypt.hash(reqBody.password, 12),
  });

  return newUser
    .save()
    .then((user) => {
      if (user) {
        return true;
      } else {
        return false;
      }
    })
    .catch((err) => err);
};

module.exports.loginUser = (reqBody) =>
  User.findOne({ email: reqBody.email }).then((result) => {
    if (!result) return false;
    else {
      const comparePassword = brcypt.compare(reqBody.password, result.password);

      if (comparePassword) return { access: auth.createAccessToken(result) };
      else return false;
    }
  });

module.exports.getProfile = (data) =>
  User.findById(data.id).then((result) => {
    if (!result) return response.status(404).send("User not found ");
    result.password = "";
    return result;
  });

module.exports.enroll = async (data, userData) => {
  if (userData.isAdmin) throw new Error("Only Users are allowed to enroll");

  let isUserUpdated = await User.findById(userData.id).then((user) => {
    // Adds the courseId in the user's enrollments array
    user.enrollments.push({ courseId: data.courseId });
    console.log(user);

    // Saves the updated user information in the database
    return user
      .save()
      .then((user) => true)
      .catch((err) => false);
  });

  // Add the user ID in the enrollees array of the course
  // Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
  let isCourseUpdated = await Course.findById(data.courseId).then((course) => {
    // Adds the userId in the course's enrollees array
    course.enrollees.push({ userId: userData.id });

    // Saves the updated course information in the database
    return course
      .save()
      .then((course) => true)
      .catch((err) => false);
  });

  // Condition that will check if the user and course documents have been updated
  // User enrollment successful

  console.log(isUserUpdated, isCourseUpdated);
  if (isUserUpdated && isCourseUpdated) {
    return true;
    // User enrollment failure
  } else {
    return false;
  }
};

module.exports.getUsers = () => User.find().then((result) => result);
