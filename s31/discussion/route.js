const http = require("http");

const server = http.createServer((request, response) => {
  if (request.url === "/greeting") {
    response.writeHead(200, {
      "Content-Type": "text/plain",
    });
    response.end("Hello Again!");
  } else if (request.url === "/homepage") {
    response.writeHead(200, {
      "Content-Type": "text/plain",
    });
    response.end("This is a homepage");
  } else {
    response.writeHead(404, {
      "Content-Type": "text/plain",
    });
    response.end("Page not found");
  }
});
