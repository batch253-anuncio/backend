const myName = "qnasdiawapsdouqe";

// If the character of your name is a vowel letter, instead of displaying the character, display "*"
// The ".toLowerCase()" function/method will change the current letter being evaluated in a loop to a lowercase letter ensuring that the letters provided in the expressions below will match
// If the letter in the name is a vowel, it will print the *
// Print in the console all non-vowel characters in the name

// for (let index = 0; index < test.length; index++) {
//   console.log(answer[index]);
// }

for (key in myName) {
  if (
    myName[key] === "a" ||
    myName[key] === "e" ||
    myName[key] === "i" ||
    myName[key] === "o" ||
    myName[key] === "u"
  ) {
    console.log(myName[key]);
  } else {
    console.log("*");
  }
}

x = 100;

for (let index = 0; index <= x; index++) {
  if (index % 3 === 0) {
    console.log(`${index} FIZZ`);
  }

  if (index % 6 === 0) {
    console.log(`${index} BUZZ`);
    console.log(`${index} FIZZBUZZ`);
  }
}
