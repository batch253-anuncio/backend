async function getAllData() {
  try {
    const response = await fetch("https://jsonplaceholder.typicode.com/todos");
    const user = await response.json();
    user.map((el) => console.log(el.title));
  } catch (error) {
    console.log(error);
  }
}

async function getData() {
  try {
    const response = await fetch(
      "https://jsonplaceholder.typicode.com/todos/1"
    );
    const user = await response.json();

    console.log(`TITLE: ${user.title}, STATUS:  ${user.completed}`);
  } catch (error) {
    console.log(error);
  }
}

async function postData() {
  const data = {
    userId: "3",
    title: "The Great Akain",
    completed: true,
  };

  try {
    const response = await fetch(
      "https://jsonplaceholder.typicode.com/todos/",
      {
        method: "POST",
        headers: { "Content-Type": "Application/JSON" },
        body: JSON.stringify(data),
      }
    );
    const user = await response.json();

    console.log(user);
  } catch (error) {
    console.log(error);
  }
}

async function putData() {
  const data = {
    userId: "3",
    title: "The Great Akain",
    completed: true,
  };

  try {
    const response = await fetch(
      "https://jsonplaceholder.typicode.com/todos/1",
      {
        method: "PUT",
        headers: { "Content-Type": "Application/JSON" },
        body: JSON.stringify(data),
      }
    );
    const user = await response.json();

    console.log(user);
  } catch (error) {
    console.log(error);
  }
}

async function updateData() {
  const data = {
    userId: "3",
    title: "The Great Akain",
    description: "Ang pinaka malakas sa mundo",
    dataCompleted: new Date().toLocaleDateString(),
    completed: true,
  };

  try {
    const response = await fetch(
      "https://jsonplaceholder.typicode.com/todos/1",
      {
        method: "PUT",
        headers: { "Content-Type": "Application/JSON" },
        body: JSON.stringify(data),
      }
    );
    const user = await response.json();

    console.log(user);
  } catch (error) {
    console.log(error);
  }
}

async function patchData() {
  const data = {
    title: "The Damielle Su",
  };

  try {
    const response = await fetch(
      "https://jsonplaceholder.typicode.com/todos/1",
      {
        method: "PATCH",
        headers: { "Content-Type": "Application/JSON" },
        body: JSON.stringify(data),
      }
    );
    const user = await response.json();

    console.log(user);
  } catch (error) {
    console.log(error);
  }
}

async function updatedData() {
  const data = {
    statusChanged: new Date().toLocaleDateString(),
  };

  try {
    const response = await fetch(
      "https://jsonplaceholder.typicode.com/todos/1",
      {
        method: "PATCH",
        headers: { "Content-Type": "Application/JSON" },
        body: JSON.stringify(data),
      }
    );
    const user = await response.json();

    console.log(user);
  } catch (error) {
    console.log(error);
  }
}

getAllData();
getData();
postData();
putData();
updateData();
patchData();
updatedData();
