const z = require('zod');

const createProductValidator = z.object({
  body: z.object({
    title: z.string({
      required_error: 'Title is required',
    }),
    category: z.string({
      required_error: 'Category is required',
    }),
    description: z.string({
      required_error: 'Item description is required',
    }),
    stock: z.number().optional(),
  }),
});

const updateProductValidator = z.object({
  params: z.object({
    id: z.string({
      required_error: 'UserId is required',
    }),
  }),
  body: z.object({
    title: z.string().optional(),
    category: z.string().optional(),
    description: z.string().optional(),
    stock: z.number().optional(),
    isActive: z.boolean().optional(),
  }),
});

const archiveValidator = z.object({
  params: z.object({
    id: z.string({
      required_error: 'UserId is required',
    }),
  }),
});
module.exports = {
  createProductValidator,
  updateProductValidator,
  archiveValidator,
};
