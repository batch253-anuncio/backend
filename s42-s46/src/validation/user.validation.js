const z = require('zod');

const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$/;

const createUserValidator = z.object({
  body: z.object({
    firstName: z.string({
      required_error: 'Name is required',
    }),
    lastName: z.string({
      required_error: 'Name is required',
    }),
    email: z
      .string({
        required_error: 'Email is required',
      })
      .email('Email is not valid'),
    password: z
      .string({
        required_error: 'Password is required',
      })
      .min(12)
      .regex(
        passwordRegex,
        'Password must meet the criteria for password strength.',
      ),

    image: z.string().optional(),
    roles: z.array().optional(),
  }),
});

const getUserValidator = z.object({
  params: z.object({
    id: z.string({
      required_error: 'UserId is required',
    }),
  }),
});

const deleteUserValdiator = z.object({
  params: z.object({
    id: z.string({
      required_error: 'UserId is required',
    }),
  }),
});

const updateUserValidator = z.object({
  params: z.object({
    id: z.string({
      required_error: 'UserId is required',
    }),
  }),
  body: z.object({
    name: z.string().optional(),
    email: z.string().email('Please enter a valid email.'),
    password: z
      .string()
      .regex(
        passwordRegex,
        'Password must meet the criteria for password strength.',
      )
      .optional(),

    image: z.string().optional(),
    roles: z.array().optional(),
  }),
});

module.exports = {
  createUserValidator,
  updateUserValidator,
  deleteUserValdiator,
  getUserValidator,
};
