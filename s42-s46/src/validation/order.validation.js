const z = require('zod');

const orderValidation = z.object({
  body: z.array(
    z.object({
      productId: z.string({
        required_error: 'Id is not required',
      }),
      title: z.string({
        required_error: 'Title is required',
      }),
      price: z.number({
        required_error: 'price is required',
      }),
      quantity: z.number({
        required_error: 'quantity is required',
      }),
    }),
  ),
});

const updateProductValidator = z.object({
  params: z.object({
    id: z.string({
      required_error: 'UserId is required',
    }),
  }),
  body: z.object({
    title: z.string().optional(),
    category: z.string().optional(),
    description: z.string().optional(),
    stock: z.number().optional(),
    isActive: z.boolean().optional(),
  }),
});

const archiveValidator = z.object({
  params: z.object({
    id: z.string({
      required_error: 'UserId is required',
    }),
  }),
});
module.exports = {
  orderValidation,
  updateProductValidator,
  archiveValidator,
};
