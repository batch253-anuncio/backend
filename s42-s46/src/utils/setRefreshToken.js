const setRefreshToken = (request, response, refreshToken) => {
  return response.cookie('jwt', refreshToken, {
    httpOnly: true,
    secure: request.secure || request.headers['x-forwarded-proto'] === 'https',
    sameSite: 'lax',
    maxAge: 576000 * 60 * 1000,
  });
};

module.exports = setRefreshToken;
