const jwt = require('jsonwebtoken');

const verifyJwt = async (token, secret) => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, secret, (error, decoded) => {
      if (error) return reject(error);

      resolve(decoded);
    });
  });
};

module.exports = verifyJwt;
