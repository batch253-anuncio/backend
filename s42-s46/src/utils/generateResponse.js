const generateResponse = (response, code, data = null) => {
  const dataToReturn = {
    sucess: 'success',
    data,
  };

  response.status(code).json(dataToReturn);
};

module.exports = generateResponse;
