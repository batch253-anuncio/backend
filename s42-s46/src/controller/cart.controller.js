const { addToCartService } = require('../service/cart.service');
const {
  requestIsEmpty,
  fetchDataIsReturnsEmptyService,
} = require('../service/product.service');

const catchAsync = require('../utils/catchAsync');
const generateResponse = require('../utils/generateResponse');
const getJwtUser = require('../utils/getUserfronToken');

const HttpSuccessCode = require('../utils/HTTPsuccesscode');

const addToCart = catchAsync(async (request, response) => {
  const { body } = request;

  requestIsEmpty(body);
  const decoded = await getJwtUser(request, response);

  fetchDataIsReturnsEmptyService(decoded);

  const newCartAdded = await addToCartService(body, decoded.id);

  return generateResponse(response, HttpSuccessCode.Created, newCartAdded);
});

const getItemsInTheCart = catchAsync(async (request, response) => {
  // const { id } = request.params;
});

module.exports = { addToCart, getItemsInTheCart };
