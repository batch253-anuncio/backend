const {
  checkOutItemsOrderedService,
  getAllOrdersSerrvice,
} = require('../service/order.service');
const {
  requestIsEmpty,
  fetchDataIsReturnsEmptyService,
} = require('../service/product.service');

const catchAsync = require('../utils/catchAsync');
const generateResponse = require('../utils/generateResponse');
const getJwtUser = require('../utils/getUserfronToken');

const HttpSuccessCode = require('../utils/HTTPsuccesscode');

const checkOutOrder = catchAsync(async (request, response) => {
  const { body } = request;

  requestIsEmpty(body);
  const decoded = await getJwtUser(request, response);

  fetchDataIsReturnsEmptyService(decoded);

  const newCartAdded = await checkOutItemsOrderedService(body, decoded.id);

  return generateResponse(response, HttpSuccessCode.Created, newCartAdded);
});

const getAllOrderedItems = catchAsync(async (request, response) => {
  const decoded = await getJwtUser(request, response);
  fetchDataIsReturnsEmptyService(decoded);
  const orderHistory = await getAllOrdersSerrvice(decoded.id);
  return generateResponse(response, HttpSuccessCode.Created, orderHistory);
});

module.exports = { checkOutOrder, getAllOrderedItems };
