// library
// const jwt = require('jsonwebtoken');
// const bcrypt = require('bcrypt');

// model
const User = require('../model/user.model');

// modules
const generateResponse = require('../utils/generateResponse');
const HttpSuccessCode = require('../utils/HTTPsuccesscode');
const {
  handleFilterRefreshToken,
  findUserAndCheckPasswordService,
  findUserByRefreshTokenService,
  handleAddRefreshToken,
  updateUserRefreshTokenService,
} = require('../service/auth.service');
const signTokens = require('../utils/signTokens');
const catchAsync = require('../utils/catchAsync');
const AppError = require('../utils/appError');
const HttpErrorCode = require('../utils/HTTPerrorcode');
const clearCookie = require('../utils/clearCookie');
const verifyJwt = require('../utils/verifyJWT');
const setRefreshToken = require('../utils/setRefreshToken');
const { omit } = require('lodash');

// sign in user using jwt
const signIn = catchAsync(async (request, response, next) => {
  const { cookies } = request;
  const { email, password } = request.body;

  const foundUser = await findUserAndCheckPasswordService(email, password);

  const { accessToken, refreshToken } = signTokens(foundUser);

  if (cookies.jwt) {
    await handleFilterRefreshToken(foundUser, cookies.jwt);
    clearCookie(request, response);
  } else {
    handleAddRefreshToken(foundUser, refreshToken);
  }

  await updateUserRefreshTokenService(foundUser);
  setRefreshToken(request, response, refreshToken);

  return generateResponse(response, HttpSuccessCode.OK, {
    roles: foundUser.roles,
    accessToken,
  });
});

// gives new accesstoken and refreshtoken
const handleRefreshToken = catchAsync(async (request, response, next) => {
  const { jwt } = request.cookies;

  try {
    if (!jwt)
      return next(new AppError('Forbidden access!', HttpErrorCode.Forbidden));

    const refreshToken = jwt;

    // clearCookie(request, response);
    const foundUser = await findUserByRefreshTokenService(refreshToken);

    const refreshTokenDuration = process.env.JWT_REFRESHTOKEN_SECRET;

    const decoded = await verifyJwt(refreshToken, refreshTokenDuration);

    // Detected refresh token reuse!
    if (!foundUser) {
      const hackedUser = await User.findById(decoded.id)
        .select('+refreshToken')
        .exec();

      if (hackedUser) {
        hackedUser.refreshToken = [];
        await hackedUser.save();
      }
      return next(new AppError('Forbidden access!', HttpErrorCode.Forbidden));
    }

    // Remove the used refresh token
    const newRefreshTokenArray = foundUser.refreshToken.filter(
      (el) => el !== refreshToken,
    );

    const { accessToken, refreshToken: newRefreshToken } =
      signTokens(foundUser);

    foundUser.refreshToken = [...newRefreshTokenArray, newRefreshToken];
    await foundUser.save();

    setRefreshToken(request, response, newRefreshToken);

    const userDataToReturn = omit(foundUser.toJSON(), [
      'refreshToken',
      'createdAt',
      'updatedAt',
      '__v',
    ]);

    return generateResponse(response, HttpSuccessCode.OK, {
      ...userDataToReturn,
      accessToken,
    });
  } catch (error) {
    console.log(error);
  }
});

// clear the cookie
const handleLogout = async (request, response) => {
  // On client, also delete the accessToken

  const cookies = request.cookies;

  if (!cookies.jwt) return response.status(204).json({ message: 'No cookies' }); // No content
  const refreshToken = cookies.jwt;

  // // Is refreshToken in db?
  const foundUser = await User.findOne({ refreshToken })
    .select('+refreshToken')
    .exec();
  if (!foundUser) {
    clearCookie(request, response);
    return response
      .status(204)
      .json({ message: 'No success logout the middle part' });
  }

  // Delete refreshToken in db
  foundUser.refreshToken = foundUser.refreshToken.filter(
    (rt) => rt !== refreshToken,
  );
  await foundUser.save();

  clearCookie(request, response);
  return response
    .status(204)
    .json({ message: 'Success Log out the last part' });
};

module.exports = { signIn, handleRefreshToken, handleLogout };
