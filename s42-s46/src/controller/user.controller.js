const User = require('../model/user.model');
const { requestIsEmpty } = require('../service/product.service');
const {
  createUserService,
  getAllUserService,
  updateUserService,
  deleteUserService,
  orderedProductsService,
} = require('../service/user-service');
const AppError = require('../utils/appError');
const catchAsync = require('../utils/catchAsync');

const generateResponse = require('../utils/generateResponse');
const getJwtUser = require('../utils/getUserfronToken');
const HttpErrorCode = require('../utils/HTTPerrorcode');
const HttpSuccessCode = require('../utils/HTTPsuccesscode');

// add a user using create
const signUp = catchAsync(async (request, response) => {
  const { body } = request;
  const user = await createUserService(body);

  return generateResponse(response, HttpSuccessCode.Created, user);
});

// Get all users
const getAllUsers = catchAsync(async (request, response) => {
  const users = await getAllUserService();
  return generateResponse(response, HttpSuccessCode.Created, users);
});

// Get a user

const getUser = catchAsync(async (request, response, next) => {
  const { id } = request.params;

  const user = await User.findById(id);
  return response.status(201).json({ data: user });
});

// update user
const updateUser = async (request, response) => {
  const { body } = request;
  const { id } = request.params;
  try {
    const user = await updateUserService(id, { body });
    return generateResponse(response, HttpSuccessCode.Created, user);
  } catch (error) {
    response.status(HttpErrorCode.BadRequest).json({ message: error });
  }
};

// delete a user
const deleteUser = async (request, response) => {
  const { id } = request.params;
  try {
    await deleteUserService(id);
    return generateResponse(response, HttpSuccessCode.Created);
  } catch (error) {
    response.status(HttpErrorCode.BadRequest).json({ message: error });
  }
};

const checkout = catchAsync(async (request, response) => {
  const { body } = request;

  requestIsEmpty(body);

  const decoded = await getJwtUser(request, response);

  if (decoded.isAdmin)
    throw new AppError(
      'Forbidden Request - Admin not allowed to purchase',
      HttpErrorCode.Forbidden,
    );

  const updatedUser = await orderedProductsService(body, decoded.id);

  return generateResponse(response, HttpSuccessCode.Created, updatedUser);
});

module.exports = {
  signUp,
  getAllUsers,
  updateUser,
  deleteUser,
  getUser,
  checkout,
};
