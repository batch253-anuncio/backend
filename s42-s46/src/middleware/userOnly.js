const AppError = require('../utils/appError');
const catchAsync = require('../utils/catchAsync');

const HttpErrorCode = require('../utils/HTTPerrorcode');
const verifyJwt = require('../utils/verifyJwt');

const userOnly = catchAsync(async (request, response, next) => {
  const authHeader =
    request.headers.authorization || request.headers.Authorization;
  if (!authHeader?.startsWith('Bearer')) return response.sendStatus(401);
  const token = authHeader.split(' ')[1];

  const decoded = await verifyJwt(token, process.env.JWT_ACCESSTOKEN_SECRET);

  if (!decoded.isAdmin) return next();

  throw new AppError(
    'Forbbiden Access - Only Users can make a purchase',
    HttpErrorCode.Forbidden,
  );
});

module.exports = userOnly;
