const jwt = require('jsonwebtoken');

const verifyToken = (request, response, next) => {
  const authHeader =
    request.headers.authorization || request.headers.Authorization;
  if (!authHeader?.startsWith('Bearer')) return response.sendStatus(401);
  const token = authHeader.split(' ')[1];

  jwt.verify(token, process.env.JWT_ACCESSTOKEN_SECRET, (err, decoded) => {
    if (err) return response.sendStatus(403); // invalid token
    request.body.id = decoded.id;
    next();
  });
};

module.exports = verifyToken;
