const HttpErrorCode = require('../utils/HTTPerrorcode');
const verifyJwt = require('../utils/verifyJwt');

const accessTokenSecret = process.env.JWT_ACCESSTOKEN_SECRET;

// const verifyRoles = (...allowedRoles) => {
//   return (request, response, next) => {
//     if (!request?.roles) return response.sendStatus(401);
//     const rolesArray = [...allowedRoles];
//     const result = request.roles.every((role, i) => role === rolesArray[i]);
//     if (!result) return response.sendStatus(401);
//     next();
//   };
// };

const adminAccessOnly = async (request, response, next) => {
  try {
    const authHeader =
      request.headers.authorization || request.headers.Authorization;
    if (!authHeader?.startsWith('Bearer')) return response.sendStatus(401);
    const token = authHeader.split(' ')[1];

    const decoded = await verifyJwt(token, accessTokenSecret);
    if (decoded.isAdmin) return next();

    return response
      .status(HttpErrorCode.Forbidden)
      .json({ message: 'Forbidden Access - Admin acess required' });
  } catch (error) {
    return response.status(400).json({ messag: error });
  }
};

module.exports = adminAccessOnly;
