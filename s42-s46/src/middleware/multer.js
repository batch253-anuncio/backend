const { CloudinaryStorage } = require('multer-storage-cloudinary');
const multer = require('multer');

const AppError = require('../utils/appError');
const HttpErrorCode = require('../utils/HTTPerrorcode');

const cloudinary = require('../config/cloudinary');

const storage = new CloudinaryStorage({
  cloudinary,
  params: async (req, file) => {
    // async code using `req` and `file`
    // ...
    return {
      folder: 'products',
    };
  },
});

const upload = multer({
  storage: storage,
  fileFilter: (request, file, cb) => {
    if (file.mimetype.startsWith('image')) {
      cb(null, true);
    } else {
      cb(
        new AppError(
          'Not an image! Only images are allowed',
          HttpErrorCode.BadRequest,
        ),
      );
    }
  },
});

module.exports.uploadSingleImage = upload.single('image');
module.exports = { upload };
