const cartRouter = require('express').Router();

const cartController = require('../controller/cart.controller');
const { addToCartValidation } = require('../validation/cart.validation');
const validate = require('../middleware/validate');
const verifyToken = require('../middleware/verifyToken');
const userOnly = require('../middleware/userOnly');

cartRouter.use(verifyToken);
cartRouter.use(userOnly);
cartRouter
  .route('/')
  .post(validate(addToCartValidation), cartController.addToCart);

module.exports = cartRouter;
