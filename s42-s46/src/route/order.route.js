const orderRouter = require('express').Router();
const orderController = require('../controller/order.controller');
const userOnly = require('../middleware/userOnly');

orderRouter.use(userOnly);
orderRouter.post('/', orderController.checkOutOrder);
orderRouter.get('/', orderController.getAllOrderedItems);

module.exports = orderRouter;
