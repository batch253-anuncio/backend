const userRouter = require('express').Router();

const validate = require('../middleware/validate');
const {
  createUserValidator,
  updateUserValidator,
  deleteUserValdiator,
  getUserValidator,
} = require('../validation/user.validation');
const userController = require('../controller/user.controller');
const authController = require('../controller/auth.controller');
// const verifyRoles = require('../middleware/verifyRoles');
const verifyToken = require('../middleware/verifyToken');

userRouter.post(
  '/signup',
  validate(createUserValidator),
  userController.signUp,
);
userRouter.get('/', userController.getAllUsers);

userRouter.post('/signin', authController.signIn);
userRouter.get('/signout', authController.handleLogout);
userRouter.get('/refresh', authController.handleRefreshToken);

userRouter.use(verifyToken);

userRouter.patch('/checkout', userController.checkout);

userRouter
  .route('/:id')
  .get(validate(getUserValidator), userController.getUser)
  .put(validate(updateUserValidator))
  .delete(validate(deleteUserValdiator));

module.exports = userRouter;
