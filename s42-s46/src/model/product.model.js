const { Schema, model } = require('mongoose');
const AppError = require('../utils/appError');

const productSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
      unique: true,
    },
    category: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    stock: {
      type: Number,
      default: 1,
    },
    image: {
      type: String,
      default: '',
    },
    productChangedAt: {
      type: Date,
    },
    isActive: {
      type: Boolean,
      default: true,
      select: false,
    },
    isInStock: {
      type: Boolean,
      default: true,
      select: false,
    },
  },
  {
    timestamps: true,
  },
);

productSchema.post('save', function (error, doc, next) {
  if (error.name === 'MongoServerError' && error.code === 11000) {
    next(new AppError(`We found a ducpliacte user for ${this.title}`));
  } else {
    next();
  }
});

const Product = model('Product', productSchema);
module.exports = Product;
