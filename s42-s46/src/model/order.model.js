const { Schema, model } = require('mongoose');

const orderSchema = new Schema({
  userId: {
    type: String,
  },

  cart: [
    {
      items: [
        {
          productId: {
            type: String,
          },
          title: {
            type: String,
          },
          quantity: {
            type: Number,
          },
          price: {
            type: Number,
          },
          subTotal: {
            type: Number,
          },
        },
      ],

      totalCost: {
        type: Number,
      },

      datePurchased: {
        type: Date,
        default: Date.now,
      },
    },
  ],
});

orderSchema.pre('save', function (next) {
  let totalCost = 0;
  this.cart.forEach((cart) => {
    cart.items.forEach((item) => {
      item.subTotal = Math.round(item.price * item.quantity * 100) / 100;
      totalCost += item.subTotal;
    });
    cart.totalCost = Math.round(totalCost * 100) / 100;
    totalCost = 0;
  });
  next();
});
const Order = model('Order', orderSchema);
module.exports = Order;
