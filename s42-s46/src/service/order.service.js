const Order = require('../model/order.model');

const checkOutItemsOrderedService = async (body, decoded) => {
  //   const newCart = new Cart({
  //     userId: decoded,
  //     cart: [
  //       {
  //         items: body,
  //       },
  //     ],
  //   });
  //   return newCart.save();

  return await Order.create({
    userId: decoded,
    cart: [
      {
        items: body,
      },
    ],
  });
};

const getAllOrdersSerrvice = async (decoded) =>
  await Order.find({ userId: decoded }).exec();

module.exports = { checkOutItemsOrderedService, getAllOrdersSerrvice };
