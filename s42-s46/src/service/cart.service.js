const Cart = require('../model/cart.model');

const addToCartService = async (body, decoded) => {
  const userCart = await Cart.findOne({ userId: decoded }).exec();

  try {
    if (userCart) {
      body.forEach((item) => {
        const index = userCart.cart[0].items.findIndex(
          (el) => el.productId === item.productId,
        );

        if (index !== -1) {
          if (item.quantity === 0)
            userCart.cart[0].items = userCart.cart[0].items.filter(
              (el) => el.productId !== item.productId,
            );
          else if (item.quantity < userCart.cart[0].items[index].quantity)
            userCart.cart[0].items[index].quantity = item.quantity;
          else userCart.cart[0].items[index].quantity = item.quantity;
        } else userCart.cart[0].items.push(item);
      });
      return await userCart.save();
    } else {
      const newCart = new Cart({
        userId: decoded,
        cart: [
          {
            items: body,
          },
        ],
      });
      return newCart.save();

      // return await Cart.create({
      //   userId: decoded,
      //   cart: [
      //     {
      //       items: body,
      //     },
      //   ],
      // });
    }
  } catch (error) {
    console.log(error);
  }
};
module.exports = { addToCartService };
