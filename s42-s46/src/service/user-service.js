const { omit } = require('lodash');
const User = require('../model/user.model');

const createUserService = async (input) => {
  const user = await User.create(input);
  const removedProps = omit(user.toJSON(), [
    'password',
    'refreshToken',
    'isActive',
    '__v',
  ]);

  return removedProps;
};

const getAllUserService = async () => {
  const user = await User.find().exec();

  return user;
};

const updateUserService = async (...data) => {
  const { id, body } = data;

  const updatedUser = await User.findByIdAndUpdate(id, { body });
  return updatedUser;
};

const deleteUserService = async (id) => {
  await User.findByIdAndDeleted(id);
};

const getUserByIDService = async (id) => await User.findById(id).exec();

const orderedProductsService = async (body, userId) => {
  const { _id, title, price, quantity } = body;

  const user = await getUserByIDService(userId);

  user.orderedProducts.products.push(
    {
      productId: _id,
      productName: title,
      quantity,

      purchasedOn: new Date().toISOString(),
    },
    { totalAmount: price * quantity },
  );

  return await user.save();
};

module.exports = {
  createUserService,
  getAllUserService,
  updateUserService,
  deleteUserService,
  orderedProductsService,
  getUserByIDService,
};
