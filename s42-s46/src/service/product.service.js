// const cloudinary = require('../config/cloudinary');

const Product = require('../model/product.model');
const AppError = require('../utils/appError');
const catchAsync = require('../utils/catchAsync');
const HttpErrorCode = require('../utils/HTTPerrorcode');

// const uploadImageToCloudinary = async (response, body) => {
//   const { image } = body;

//   try {
//     const imagesArrCloudinary = await Promise.all(
//       body.image.map((image) =>
//         cloudinary.uploader.upload(image.img, {
//           upload_preset: process.env.CLOUDINARY_UPLOAD_PRESET_SIGNED,
//           folder: 'products',
//           allowed_formats: ['jpg', 'png', 'jpeg', 'webp'],
//           unique_filename: true,
//           tags: [body.title],
//         }),
//       ),
//     );
//     if (imagesArrCloudinary) {
//       body.image = imagesArrCloudinary.map((image) => image.secure_url);
//       return body;
//     }
//   } catch (error) {
//     console.log(error);
//     response.status(500).send(error);
//   }
// };

const getProductService = catchAsync(async (id) => {
  const product = await Product.findById(id);

  if (!product)
    throw new AppError('No Product Found', HttpErrorCode.BadRequest);

  return product;
});

const checkItemDuplicate = async (data) => {
  const { title } = data;

  const checkDuplicate = await Product.findOne({ title }).exec();

  if (checkDuplicate)
    throw new AppError('Product already exist', HttpErrorCode.BadRequest);
};

const requestIsEmpty = (data) => {
  if (!data)
    throw new AppError('No product data found', HttpErrorCode.BadRequest);
};

const fetchDataIsReturnsEmptyService = (data, next) => {
  if (!data)
    throw next(new AppError('No product data found', HttpErrorCode.BadRequest));
};

const deleteItemService = async (id) => {
  const findProduct = await Product.findById(id).exec;

  fetchDataIsReturnsEmptyService(findProduct);

  return await Product.findByIdAndUpdate(id, { isActive: false })
    .select('+isActive')
    .exec();
};

const updateProductService = async (id, body) => {
  const checkProduct = await Product.findById(id).exec();
  fetchDataIsReturnsEmptyService(checkProduct);

  return await Product.findByIdAndUpdate(id, body, { new: true })
    .select('+isActive')
    .exec();
};

const getArchivedProducts = async (data) => {
  return await Product.find({ isActive: data }).select('+isActive').exec();
};

const saveProductstoDBService = async (data) => await Product.create(data);

const getAllProductService = async () => await Product.find();

const getProductByCategoryService = async (category) =>
  await Product.findOne({ category });

const getProductByIdService = async (id) => await Product.findById(id);

module.exports = {
  // uploadImageToCloudinary,
  saveProductstoDBService,
  getAllProductService,
  getProductService,
  checkItemDuplicate,
  requestIsEmpty,
  getProductByCategoryService,
  fetchDataIsReturnsEmptyService,
  getProductByIdService,
  deleteItemService,
  getArchivedProducts,
  updateProductService,
};
