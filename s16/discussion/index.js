let x = 81;
y = 9;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y; // asterisk for multiplation
console.log("Result of multiplication operator: " + product);

let quotient = x / y; // forward slash for division
console.log("Result of division operator: " + quotient);

let remainder = x % y; // percent for remainder
console.log("Result of modulo operator: " + remainder);

// [SECTION] Assignment Operators
// Basic Assignment Operator (=)
// The assignment operator assigns the value of the **right** operand to a variable.

let assignmentNumber = 8;
