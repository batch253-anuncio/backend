const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 4000;

// ======= MongoDB Connection ========= //
mongoose.connect(
  "mongodb+srv://admin:admin123@b253-anuncio.kilox2p.mongodb.net/s35?retryWrites=true&w=majority",

  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () =>
  console.log(`We're now connected to the cloud database: MongoDB Atlas!`)
);

// ======= schema ========= //

const taskSchema = new mongoose.Schema({
  name: String,
  status: {
    type: String,
    default: "pending",
  },
});

const userSchema = new mongoose.Schema({
  username: String,
  password: String,
});

// ======= schema ========= //

// ======= model ========= //

const Task = mongoose.model("Task", taskSchema);
const User = mongoose.model("User", userSchema);

// ======= model ========= //

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// ===== //
app.post("/tasks", (req, res) => {
  Task.findOne({ name: req.body.name })
    .then((result) => {
      if (result != null && result.name == req.body.name) {
        return res.send("Duplicate task found.");
      } else {
        let newTask = new Task({
          name: req.body.name,
        });

        newTask
          .save()
          .then((result) => res.send(result))
          .catch((error) => res.send(error));
      }
    })
    .catch((error) => res.send(error));
});

// ===== //

app.get("/tasks", (req, res) => {
  Task.find({})
    .then((result) => res.status(200).send(result))
    .catch((error) => res.send(error));
});

// ===== //

app.get("/users", (req, res) => {
  User.find({})
    .then((result) => res.status(200).send(result))
    .catch((error) => res.send(error));
});

// ===== //

app.post("/signup", (req, res) => {
  User.findOne({ username: req.body.username })
    .then((result) => {
      if (result != null && result.username == req.body.username) {
        return res.send("Duplicate username found.");
      } else {
        if (req.body.username != "" && req.body.password != "") {
          let newUser = new User({
            username: req.body.username,
            password: req.body.password,
          });
          newUser
            .save()
            .then((result) =>
              res
                .status(201)
                .send(`New user registered. Username: ${req.body.username}`)
            )
            .catch((error) => res.send(error));
        } else {
          return res.send("BOTH username and password must be provided.");
        }
      }
    })
    .catch((error) => res.send(error));
});

app.listen(port, () => console.log(`Server running at localhost:${port}...`));
