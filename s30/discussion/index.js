// Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data

db.fruits.aggregate([
  {
    $match: {
      onSale: true,
    },
  },
  {
    $group: {
      _id: "$supplier_id",
      total: { $sum: "$stock" },
    },
  },
]);

/*
	- The "$match" is used to pass the documents that meet the specified conditions
*/
/*
- The "$group" is used to group elements together and field-value pairs using the data from the grouped elements */

// Field Projection with Aggregation
// $project can be used aggregating data to include/exclude field from the returned results
/*
    Syntax:
        { $project: { "field": 1/0 }}
*/

db.fruits.aggregate([
  {
    $match: {
      onSale: true,
    },
  },
  {
    $group: {
      _id: "$supplier_id",
      total: { $sum: "$stock" },
    },
  },
  {
    $project: {
      _id: 0,
    },
  },
]);

db.fruits.aggregate([
  {
    $match: {
      onSale: true,
    },
  },
  {
    $group: {
      _id: "$supplier_id",
      total: { $min: "$stock" },
    },
  },
  {
    $sort: { total: -1 },
  },
]);

// Computations for aggregated results
// used in $group staging
/*
	$sum - to total the values
	$avg - to total the average of values
	$min - minimum value of the field
	$max - maximum value of the field

	Syntax:
		$group: { 
			"_id": "fieldToBeGrouped",
			"fieldForResults": { $sum/max/min/avg: "$stock"} 
		}

*/
db.fruits.aggregate([{ $unwind: "$origin" }]);

db.fruits.aggregate([
  { $unwind: "$origin" },
  {
    $group: {
      _id: "$origin",
      kinds: { $sum: 1 },
    },
  },
]);
