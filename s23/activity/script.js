console.log("HELLO POKEMON");

// console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added

// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method

// Create a constructor function called Pokemon for creating a pokemon

// Create/instantiate a new pokemon

// Create/instantiate a new pokemon

// Create/instantiate a new pokemon

// Invoke the tackle method and target a different object

// Invoke the tackle method and target a different object

let trainer = {
  name: "Ash Kechum",
  age: 10,
  pokemon: ["Pickachu", "Charizard", "Squirtle", "Bulbasaur"],
  friends: { hoenn: ["May", "Max"], kanto: ["Brock", "Misty"] },
  talk: function () {
    return `${this.pokemon[0]} I choose you`;
  },
};

console.log("Result of dot notation: ");
console.log(trainer.name);

console.log("Result of square bracket notation: ");
console.log(trainer["pokemon"]);

console.log("Result of talk method ");
console.log(trainer.talk());

//constructor function
function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = level * 2;
  this.attack = level;
  this.tackle = (pokemon) => {
    let damagedHealth = pokemon.health - this.attack;
    console.log(`${this.name} tackled ${pokemon.name}`);
    console.log(`${pokemon.name}'s health  is now reduced to ${damagedHealth}`);
    pokemon.health = damagedHealth;

    if (pokemon.health <= 0) pokemon.faint();
    console.log(pokemon);
  };
  this.faint = () => {
    console.log(`${this.name} fainted`);
  };
}

const Pikachu = new Pokemon("Pikachu", 12);
const Geodude = new Pokemon("Geodude", 8);
const Mewtwo = new Pokemon("Mewtow", 100);

console.log(Pikachu);
console.log(Geodude);
console.log(Mewtwo);

Geodude.tackle(Pikachu);
Mewtwo.tackle(Geodude);

//Do not modify
//For exporting to test.js
try {
  module.exports = {
    trainer,
    Pokemon,
  };
} catch (err) {}
